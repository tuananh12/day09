<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Form.css">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<?php 
     if ($_SERVER['REQUEST_METHOD'] == "POST") // Gửi yêu cầu cho sever biết khi nhấn nút đăng kí
     {
        $_COOKIE = $_POST;
        
        $q1=  $_COOKIE['question-1-answers'];
        $q2=  $_COOKIE['question-2-answers'];
        $q3=  $_COOKIE['question-3-answers'];
        $q4=  $_COOKIE['question-4-answers'];
        $q5=  $_COOKIE['question-5-answers'];
     
     
        if (isset($_POST['Next'])) 
          {  
                setcookie( 'question-1-answers', $q1);
                setcookie( 'question-2-answers', $q2);
                setcookie( 'question-3-answers', $q3);
                setcookie( 'question-4-answers', $q4);
                setcookie( 'question-5-answers', $q5);

                header('location: form2.php');

          }

                    }

    ?>
<body>
<form action='' method='POST'>
    <div class="containerr  my-1">
        <H1 class="text-center">Trang 1</H1>
        
        
        
        <br>
        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 h5"><b>1 .Khoa học nhận định rằng trà xanh giảm cân tốt hơn cà phê, vậy 1 kg trà và 1 kg cà phê cái nào nặng hơn?</b></div>
            
        <?php
          

         
        
        
        $question = array( "A" => "Bằng nhau", "B" => "Trà xanh","C" => "Cà phê","D" => "Không biết");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </Br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='question-1-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-1-answers']) && $_COOKIE['question-1-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>2 .Một cây bút chì bình thường viết được mấy đầu?</b></div>
            
        <?php
        
        
        
        $question = array( "A" => "1 đầu", "B" => "2 Đầu","C" => "3 đầu","D" => "4 đầu");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='question-2-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-2-answers']) && $_COOKIE['question-2-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>3 .Muốn đổ xăng vào cây xăng, vậy muốn đổ dầu vào đâu?</b></div>
            
        <?php
        
        
        
        $question = array( "A" => "Cây xăng", "B" => "Siêu thị","C" => "Điện máy","D" => "Tạp hóa");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='question-3-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-3-answers']) && $_COOKIE['question-3-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>4 .Trong bài “Ghen” do Erik và Min thể hiện. Chàng trai gọi cho cô gái lúc mấy giờ?</b></div>
            
        <?php
        
        
        
        $question = array( "A" => "Không gọi", "B" => "1 giờ sáng","C" => "2 giờ sáng","D" => "Không biết");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='question-4-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-4-answers']) && $_COOKIE['question-4-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>5 .Chùa một cột xây dựng dựa trên hình tượng loại hoa nào? </b></div>
            
        <?php
        
        
        
        $question = array( "A" => "Hoa cải đỏ", "B" => "Hoa hồng","C" => "Hoa loa kèn","D" => "Hoa sen");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='question-5-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-5-answers']) && $_COOKIE['question-5-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 
        
                
        
         
                
        
        <div class="d-flex align-items-center pt-3">

            <div class="ml-auto mr-sm-5  Next1">
                
                <input class="btn btn-success" name='Next' type="submit" value="Next"></input>
            </div>
        </div>
        
    </div>
    
</body>
</html>