<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Form.css">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<body>
<form action='' method='POST'>
    <div class="containerr  my-1">
        <H1 class="text-center">Kết quả</H1>
        
        <?php
  $totalscore = 0;
  $keys = array("question-1-answers" => "A", "question-2-answers" => "B", "question-3-answers" => "A", "question-4-answers" => "C", "question-5-answers" => "D", "question-6-answers" => "B", "question-7-answers" => "B", "question-8-answers" => "D", "question-9-answers" => "C", "question-10-answers" => "B");
  
  $msg = "Bạn quá kém, cần ôn tập thêm.";
foreach (array_keys($keys) as $k) {
    if ($keys[$k] == $_COOKIE[$k]) {
        ++$totalscore;
    }
}


   echo "<div id='results'>Bạn đã trả lời đúng $totalscore /10</div>";

   if($totalscore <= 4) {
    echo "Bạn quá kém,cần ôn tập thêm";
   }

   if($totalscore > 4 && $totalscore < 7) {
    echo "Cũng bình thường";
   }

   if($totalscore >= 7) {
    echo "Sắp sửa làm được trợ giảng lớp PHP";
   }
   
 ?>
        <div class="d-flex align-items-center pt-3">
            <div id="" class="" >
                
                <a class="btn btn-primary" href="Form1.php">Làm lại</a>
            </div>
            
        </div>
    </div>
</body>
</html>