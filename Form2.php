<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Form.css">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<body>
<?php 
     if ($_SERVER['REQUEST_METHOD'] == "POST") // Gửi yêu cầu cho sever biết khi nhấn nút đăng kí
     {
        $_COOKIE = $_POST;
        
        $q6=  $_COOKIE['question-6-answers'];
        $q7=  $_COOKIE['question-7-answers'];
        $q8=  $_COOKIE['question-8-answers'];
        $q9=  $_COOKIE['question-9-answers'];
        $q10=  $_COOKIE['question-10-answers'];
     
     
        if (isset($_POST['Nopbai']) ) 
          {  

                setcookie( 'question-6-answers', $q6);
                setcookie( 'question-7-answers', $q7);
                setcookie( 'question-8-answers', $q8); 
                setcookie( 'question-9-answers', $q9);
                setcookie( 'question-10-answers', $q10);

                header('location: form3.php');

          }

    
                    }
    
  
                
   

    ?>
<form action='' method='POST'>
    <div class="containerr  my-1">

        <H1 class="text-center">Trang 2</H1>
        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 h5"><b>6 .Bạch mã là con ngựa trắng, hắc mã là con ngựa đen, vậy ngựa ô là con ngựa gì?</b></div>
            
        <?php
        
        
        
        $gender = array( "A" => "Ngựa che ô", "B" => "Ngựa đen","C" => "Ngựa nâu","D" => "Không biết");
        foreach ($gender as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='gender' name='question-6-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-6-answers']) && $_COOKIE['question-6-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>7 .Một cậu bé đi xe đạp sắp bị ngã thì ai đỡ cậu ấy?</b></div>
            
        <?php
        
        
        
        $gender = array( "A" => "Bố", "B" => "Sắp ngã chứ chưa ngã","C" => "Không ai đỡ","D" => "Tôi đỡ");
        foreach ($gender as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='gender' name='question-7-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-7-answers']) && $_COOKIE['question-7-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>8 .Người đàn ông duy nhất trên thế giới có sữa là ai?</b></div>
            
        <?php
        
        
        
        $gender = array( "A" => "Ông già Noel", "B" => "Ông Phúc","C" => "Ông Lộc","D" => "Ông Thọ");
        foreach ($gender as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='gender' name='question-8-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-8-answers']) && $_COOKIE['question-8-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>9 .Nhà Nam có 4 anh chị em, 3 người lớn tên là Xuân, Hạ, Thu, người em út tên gì?</b></div>
            
        <?php
        
        
        
        $gender = array( "A" => "Đông", "B" => "Xuân Anh","C" => "Nam","D" => "Tùng Dương");
        foreach ($gender as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='gender' name='question-9-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-9-answers']) && $_COOKIE['question-9-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>10 . Bệnh gì bác sỹ bó tay?</b></div>
            
        <?php
        
        
        
        $gender = array( "A" => "Ung thư", "B" => "Gãy tay","C" => "HIV","D" => "Không biết");
        foreach ($gender as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='gender' name='question-10-answers' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['question-10-answers']) && $_COOKIE['question-10-answers'] == $i ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div>
        <div class="d-flex align-items-center pt-3">
            
            <div class="ml-auto mr-sm-5 Next1">
                
                <!-- <a class="btn btn-success" href="Form3.php">Nộp bài</a> -->
                <input class="btn btn-success" type="submit" name="Nopbai" value="Nộp bài" />
            </div>
        </div>
        </form>
    </div>
</body>
</html>